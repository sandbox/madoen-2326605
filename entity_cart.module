<?php

define('ENTITY_CART_ORDERS_CONFIRMED', 1);
define('ENTITY_CART_ORDERS_NOT_CONFIRMED', 0);

/**
 * Implements hook_entity_info().
 *
 * This is the fundamental description of the entity.
 *
 * It provides a single entity with a single bundle and without revision
 * support.
 */
function entity_cart_entity_info() {
  $info['entity_cart'] = array(
    // A human readable label to identify our entity.
    'label' => t('Entity Cart'),

    // The controller for our Entity, extending the Drupal core controller.
    'controller class' => 'EntityCartController',

    // The table for this entity defined in hook_schema()
    'base table' => 'entity_cart',

    // Returns the uri elements of an entity.
    'uri callback' => 'entity_cart_uri',

    // IF fieldable == FALSE, we can't attach fields.
    'fieldable' => TRUE,

    // entity_keys tells the controller what database fields are used for key
    // functions. It is not required if we don't have bundles or revisions.
    // Here we do not support a revision, so that entity key is omitted.
    'entity keys' => array(
      // The 'id' (ecid here) is the unique id.
      'id' => 'ecid' ,
      // Bundle will be determined by the 'bundle_type' field.
      'bundle' => 'bundle_type',
    ),
    'bundle keys' => array(
      'bundle' => 'bundle_type',
    ),

    // FALSE disables caching. Caching functionality is handled by Drupal core.
    'static cache' => TRUE,

    // Bundles are alternative groups of fields or configuration
    // associated with a base entity type.
    'bundles' => array(
      'order' => array(
        'label' => 'Order',
        // 'admin' key is used by the Field UI to provide field and
        // display UI pages.
        'admin' => array(
          'path' => 'admin/structure/entity_cart/manage',
          'access arguments' => array('administer entity_cart entities'),
        ),
      ),
    ),
    // View modes allow entities to be displayed differently based on context.
    // As a demonstration we'll support "Tweaky", but we could have and support
    // multiple display modes.
    'view modes' => array(
      'tweaky' => array(
        'label' => t('Tweaky'),
        'custom settings' => FALSE,
      ),
    ),
  );

  return $info;
}

/**
 * Fetch a basic object.
 *
 * This function ends up being a shim between the menu system and
 * entity_cart_load_multiple().
 *
 * This function gets its name from the menu system's wildcard
 * naming conventions. For example, /path/%wildcard would end
 * up calling wildcard_load(%wildcard value). In our case defining
 * the path: order/%entity_cart in
 * hook_menu() tells Drupal to call entity_cart_load().
 *
 * @param int $ecid
 *   Integer specifying the basic entity id.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return object
 *   A fully-loaded $entity object or FALSE if it cannot be loaded.
 *
 * @see entity_cart_load_multiple()
 * @see entity_cart_menu()
 */
function entity_cart_load($ecid = NULL, $reset = FALSE) {
  $ecids = (isset($ecid) ? array($ecid) : array());
  $entity = entity_cart_load_multiple($ecids, array(), $reset);
  return $entity ? reset($entity) : FALSE;
}

/**
 * Loads multiple basic entities.
 *
 * We only need to pass this request along to entity_load(), which
 * will in turn call the load() method of our entity controller class.
 */
function entity_cart_load_multiple($ecids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('entity_cart', $ecids, $conditions, $reset);
}

/**
 * Implements the uri callback.
 */
function entity_cart_uri($entity) {
  return array(
    'path' => 'order/' . $entity->ecid,
  );
}

/**
 * Implements hook_menu().
 */
function entity_cart_menu() {
  // Add an order entities.
  $items['order/add'] = array(
    'title' => 'Add an order',
    'page callback' => 'entity_cart_add',
    'access arguments' => array('create entity_cart entities'),
    'type' => MENU_CALLBACK,
  );

  // The page to view our entities - needs to follow what
  // is defined in basic_uri and will use load_basic to retrieve
  // the necessary entity info.
  $items['order/%entity_cart'] = array(
    'title callback' => 'entity_cart_title',
    'title arguments' => array(1),
    'page callback' => 'entity_cart_view',
    'page arguments' => array(1),
    'access arguments' => array('view any entity_cart entity'),
  );

  // 'View' tab for an individual entity page.
  $items['order/%entity_cart/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  // 'Edit' tab for an individual entity page.
  $items['order/%entity_cart/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_cart_form', 1),
    'access arguments' => array('edit any entity_cart entity'),
    'type' => MENU_LOCAL_TASK,
  );

  // This provides a place for Field API to hang its own
  // interface and has to be the same as what was defined
  // in order_info() above.
  $items['admin/structure/entity_cart/manage'] = array(
    'title' => 'Entity cart',
    'page callback' => 'entity_cart_info_page',
    'access arguments' => array('administer entity_cart entities'),
    'file' => 'includes/entity_cart.list.inc',
  );

  $items['admin/content/entity_cart'] = array(
    'title' => 'Entity cart',
    'page callback' => 'entity_cart_list_entities',
    'access arguments' => array('administer entity_cart entities'),
    'file' => 'includes/entity_cart.list.inc',
  );

  // List of all entity_cart entities.
  $items['admin/content/entity_cart/unconfirmed'] = array(
    'title callback' => 'entity_cart_orders_count_unconfirmed',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -5,
  );

  $items['admin/content/entity_cart/confirmed'] = array(
    'title' => 'Confirmed',
    'page arguments' => array('confirmed'),
    'access arguments' => array('view any entity_cart entity'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function entity_cart_permission() {
  $permissions = array(
    'administer entity_cart entities' => array(
      'title' => t('Administer Entity cart entities'),
    ),
    'view any entity_cart entity' => array(
      'title' => t('View any Entity cart entity'),
    ),
    'edit any entity_cart entity' => array(
      'title' => t('Edit any Entity cart entity'),
    ),
    'create entity_cart entities' => array(
      'title' => t('Create Entity cart Entities'),
    ),
  );
  return $permissions;
}

/**
 * Callback for a page title when this entity is displayed.
 */
function entity_cart_title($entity) {
  return t('Order id: @ecid', array('@ecid' => $entity->ecid));
}

/**
 * Menu callback to display an entity.
 *
 * As we load the entity for display, we're responsible for invoking a number
 * of hooks in their proper order.
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 * @see hook_entity_view_alter()
 */
function entity_cart_view($entity, $view_mode = 'tweaky') {
  // Our entity type, for convenience.
  $entity_type = 'entity_cart';
  // Start setting up the content.
  $entity->content = array(
    '#view_mode' => $view_mode,
  );
  // Build fields content - this is where the Field API really comes in to play.
  // The task has very little code here because it all gets taken care of by
  // field module.
  // field_attach_prepare_view() lets the fields load any data they need
  // before viewing.
  field_attach_prepare_view($entity_type, array($entity->ecid => $entity),
    $view_mode);
  // We call entity_prepare_view() so it can invoke hook_entity_prepare_view()
  // for us.
  entity_prepare_view($entity_type, array($entity->ecid => $entity));
  // Now field_attach_view() generates the content for the fields.
  $entity->content += field_attach_view($entity_type, $entity, $view_mode);

  // OK, Field API done, now we can set up some of our own data.
  $entity->content['created'] = array(
    '#type' => 'item',
    '#title' => t('Created date'),
    '#markup' => format_date($entity->created),
  );
  $entity->content['name'] = array(
    '#type' => 'item',
    '#title' => t('Name'),
    '#markup' => $entity->name,
  );
  $entity->content['mail'] = array(
    '#type' => 'item',
    '#title' => t('Email address'),
    '#markup' => $entity->mail,
  );

  // Now to invoke some hooks. We need the language code for
  // hook_entity_view(), so let's get that.
  global $language;
  $langcode = $language->language;
  // And now invoke hook_entity_view().
  module_invoke_all('entity_view', $entity, $entity_type, $view_mode,
    $langcode);
  // Now invoke hook_entity_view_alter().
  drupal_alter(array('entity_cart_view', 'entity_view'),
    $entity->content, $entity_type);

  // And finally return the content.
  return $entity->content;
}

/**
 * Implements hook_field_extra_fields().
 *
 * This exposes the "extra fields" (usually properties that can be configured
 * as if they were fields) of the entity as pseudo-fields
 * so that they get handled by the Entity and Field core functionality.
 * Node titles get treated in a similar manner.
 */
function entity_cart_field_extra_fields() {
  $form_elements['mail'] = array(
    'label' => t('Email address'),
    'description' => t('Buyer email address'),
    'weight' => -5,
  );
  $form_elements['name'] = array(
    'label' => t('Name'),
    'description' => t('Buyer name'),
    'weight' => -5,
  );
  $display_elements['mail'] = array(
    'label' => t('Email address'),
    'description' => t('Buyer email address'),
    'weight' => 0,
  );
  $display_elements['name'] = array(
    'label' => t('Name'),
    'description' => t('Buyer name'),
    'weight' => 0,
  );
  $display_elements['created'] = array(
    'label' => t('Creation date'),
    'description' => t('Creation date'),
    'weight' => 0,
  );

  // Since we have only one bundle type, we'll just provide the extra_fields
  // for it here.
  $extra_fields['entity_cart']['order']['form'] = $form_elements;
  $extra_fields['entity_cart']['order']['display'] = $display_elements;

  return $extra_fields;
}

/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function entity_cart_add() {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('entity_cart')->create();
  return drupal_get_form('entity_cart_form', $entity);
}

/**
 * Form function to create an entity_cart entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function entity_cart_form($form, &$form_state, $entity) {
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#required' => TRUE,
    '#default_value' => $entity->mail,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $entity->name,
  );

  $form['order'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form('entity_cart', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  if (!empty($entity->ecid)) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('entity_cart_edit_delete'),
      '#weight' => 200,
    );
  }

  return $form;
}

/**
 * Validation handler for entity_cart_add_form form.
 *
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function entity_cart_form_validate($form, &$form_state) {
  if (!preg_match("/^[a-zA-Z ]*$/",$form_state['values']['name'])) {
    form_set_error('name', t('Only letters and white space allowed'));
  }
  if (!filter_var($form_state['values']['mail'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('mail', t('Invalid email format'));
  }
  field_attach_form_validate('entity_cart', $form_state['values']['order'], $form, $form_state);
}

/**
 * Form submit handler: Submits basic_add_form information.
 */
function entity_cart_form_submit($form, &$form_state) {
  $entity = $form_state['values']['order'];
  $entity->name = $form_state['values']['name'];
  $entity->mail = $form_state['values']['mail'];
  field_attach_submit('entity_cart', $entity, $form, $form_state);
  $entity = entity_cart_save($entity);
  $form_state['redirect'] = 'order/' . $entity->ecid;
}

/**
 * Form deletion handler.
 *
 * @todo: 'Are you sure?' message.
 */
function entity_cart_edit_delete($form, &$form_state) {
  $entity = $form_state['values']['order'];
  entity_cart_delete($entity);
  drupal_set_message(t('Order id: %id has been deleted',
    array('%name' => $entity->name, '%id' => $entity->ecid))
  );
  $form_state['redirect'] = 'admin/content/entity_cart';
}

/**
 * We save the entity by calling the controller.
 */
function entity_cart_save(&$entity) {
  return entity_get_controller('entity_cart')->save($entity);
}

/**
 * Use the controller to delete the entity.
 */
function entity_cart_delete($entity, $batch = FALSE) {
  entity_get_controller('entity_cart')->delete($entity);
  if($batch) {
    $_SESSION['http_request_count']++;
  }
}

/**
 * @} End of "defgroup entity_cart".
 */
