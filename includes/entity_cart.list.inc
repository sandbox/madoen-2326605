<?php

/**
 * @file
 * The file for admin forms and functionality for the entity_cart entity
 */

/**
 * Returns a menu title which includes the number of unconfirmed order.
 */
function entity_cart_orders_count_unconfirmed() {
  $count = db_query('SELECT COUNT(ecid) FROM {entity_cart} WHERE status = :status', array(
    ':status' => ENTITY_CART_ORDERS_NOT_CONFIRMED,
  ))->fetchField();
  return t('Unconfirmed orders (@count)', array('@count' => $count));
}

/**
 * Basic information for the page.
 */
function entity_cart_info_page() {
  $content['preface']['#markup'] = t('Here you can administer these entity !fields and change the !display.',
    array(
      '!fields' => l(t('fields'), 'admin/structure/entity_cart/manage/fields'),
      '!display' => l(t('display'), 'admin/structure/entity_cart/manage/display'),
    )
  );

  return $content;
}

/**
 * Menu callback; present an administrative entity_cart listing.
 */
function entity_cart_list_entities($type = 'unconfirmed') {
  $edit = $_POST;

  if (isset($edit['action']) && ($edit['action'] == 'delete') && isset($edit['entity_carts']) && $edit['entity_carts']) {
    return drupal_get_form('entity_cart_multiple_delete_confirm');
  }
  else {
    return drupal_get_form('entity_cart_admin_overview', $type);
  }
}

/**
 * Form builder for the entity_cart overview administration form.
 *
 * @param $arg
 *   Current path's fourth component: the type of overview form ('unconfirmed' or 'confirmed').
 *
 * @ingroup forms
 * @see entity_cart_admin_overview_validate()
 * @see entity_cart_admin_overview_submit()
 * @see theme_entity_cart_admin_overview()
 */
function entity_cart_admin_overview($form, &$form_state, $arg) {
  // Build a 'Search function' form.
  $ecid = !empty($_GET['ecid']) ? $_GET['ecid'] : NULL;
  $name = !empty($_GET['name']) ? $_GET['name'] : NULL;
  $mail = !empty($_GET['mail']) ? $_GET['mail'] : NULL;

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#attributes' => array('class' => array('container-inline')),
  );

  $form['search']['ecid'] = array(
    '#type' => 'textfield',
    '#title' => t('ID'),
    '#maxlength' => 10,
    '#size' => 5,
    '#default_value' => !empty($ecid) ? $ecid : '',
  );

  $form['search']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => 25,
    '#size' => 15,
    '#default_value' => !empty($name) ? $name : '',
  );

  $form['search']['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#maxlength' => 255,
    '#size' => 30,
    '#default_value' => !empty($mail) ? $mail : '',
  );

  $form['search']['search_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  // Build an 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );

  if ($arg == 'unconfirmed') {
    $options['confirmed'] = t('Confirm the selected orders');
  }
  else {
    $options['unconfirmed'] = t('Cancel the selected orders');
  }
  $options['delete'] = t('Delete the selected orders');

  $form['options']['action'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'publish',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  // Load the orders that need to be displayed.
  $status = ($arg == 'unconfirmed') ? ENTITY_CART_ORDERS_NOT_CONFIRMED : ENTITY_CART_ORDERS_CONFIRMED;
  $header = array(
    'ecid' => array('data' => t('ID'), 'field' => 'o.ecid'),
    'name' => array('data' => t('Name'), 'field' => 'o.name'),
    'mail' => array('data' => t('Email'), 'field' => 'o.mail'),
    'created' => array('data' => t('Created'), 'field' => 'o.created'),
    'updated' => array('data' => t('Updated'), 'field' => 'o.updated', 'sort' => 'desc'),
    'action' => array('data' => t('Action')),
  );

  $query = db_select('entity_cart', 'o')->extend('PagerDefault')->extend('TableSort');
  if (!empty($ecid)) {
    $query->condition('ecid', $ecid, '=');
  }
  if (!empty($name)) {
    $query->condition('name', '%' . $name . '%', 'like');
  }
  if (!empty($mail)) {
    $query->condition('mail', '%' . $mail . '%', 'like');
  }
  $result = $query
    ->fields('o', array('ecid', 'name', 'mail', 'created', 'updated'))
    ->condition('o.status', $status)
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  $ecids = array();

  // We collect a sorted list of node_titles during the query to attach to the
  // entity_carts later.
  foreach ($result as $row) {
    $ecids[] = $row->ecid;
  }
  $entity_carts = entity_cart_load_multiple($ecids);

  // Build a table listing the appropriate entity_carts.
  $options = array();
  $destination = drupal_get_destination();

  foreach ($entity_carts as $entity_cart) {
    $options[$entity_cart->ecid] = array(
      'ecid' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $entity_cart->ecid,
          '#href' => 'order/' . $entity_cart->ecid,
          '#options' => array('attributes' => array('title' => 'entity_cart-' . $entity_cart->ecid), 'fragment' => 'entity_cart-' . $entity_cart->ecid),
        ),
      ),
      'name' => $entity_cart->name,
      'mail' => $entity_cart->mail,
      'created' => format_date($entity_cart->created, 'short'),
      'updated' => format_date($entity_cart->updated, 'short'),
      'action' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('edit'),
          '#href' => 'order/' . $entity_cart->ecid . '/edit',
          '#options' => array('query' => $destination),
        ),
      ),
    );
  }

  $form['entity_carts'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No order available.'),
  );

  $form['pager'] = array('#theme' => 'pager');

  return $form;
}

/**
 * Validate entity_cart_admin_overview form submissions.
 */
function entity_cart_admin_overview_validate($form, &$form_state) {
  $form_state['values']['entity_carts'] = array_diff($form_state['values']['entity_carts'], array(0));
  // We can't execute any 'Update options' if no orders were selected.
  if (count($form_state['values']['entity_carts']) == 0 && (empty($form_state['values']['ecid']) && empty($form_state['values']['name']) && empty($form_state['values']['mail']))) {
    form_set_error('', t('Select one or more orders to perform the update on.'));
  }
}

/**
 * Process entity_cart_admin_overview form submissions.
 *
 * Execute the chosen 'Update option' on the selected entity_carts, such as
 * publishing, unpublishing or deleting.
 */
function entity_cart_admin_overview_submit($form, &$form_state) {
  $action = $form_state['values']['action'];
  $ecids = $form_state['values']['entity_carts'];

  if ($action == 'delete') {
    entity_cart_delete_multiple($ecids);
  }
  else {
    foreach ($ecids as $ecid => $value) {
      $entity_cart = entity_cart_load($value);

      if ($action == 'unconfirmed') {
        $entity_cart->status = ENTITY_CART_ORDERS_NOT_CONFIRMED;
        $entity_cart->updated = REQUEST_TIME;
        $redirect = 'unconfirmed';
      }
      elseif ($action == 'confirmed') {
        $entity_cart->status = ENTITY_CART_ORDERS_CONFIRMED;
        $entity_cart->updated = REQUEST_TIME;
        $redirect = 'confirmed';
      }
      entity_cart_save($entity_cart);
    }
  }
  if (!empty($form_state['values']['ecid']) || !empty($form_state['values']['name']) || !empty($form_state['values']['mail'])) {
    if ($form_state['build_info']['args'][0] == 'confirmed') {
      drupal_goto('admin/content/entity_cart/confirmed', array('query' => array('ecid' => $form_state['values']['ecid'], 'name' => $form_state['values']['name'], 'mail' => $form_state['values']['mail'])));
    }
    if ($form_state['build_info']['args'][0] == 'unconfirmed') {
      drupal_goto('admin/content/entity_cart/unconfirmed', array('query' => array('ecid' => $form_state['values']['ecid'], 'name' => $form_state['values']['name'], 'mail' => $form_state['values']['mail'])));
    }
  }
  drupal_set_message(t('The update has been performed.'));
  $form_state['redirect'] = 'admin/content/entity_cart/' .$redirect;
  cache_clear_all();
}

/**
 * List the selected entity_carts and verify that the admin wants to delete them.
 *
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @return
 *   TRUE if the entity_carts should be deleted, FALSE otherwise.
 * @ingroup forms
 * @see entity_cart_multiple_delete_confirm_submit()
 */
function entity_cart_multiple_delete_confirm($form, &$form_state) {
  $edit = $form_state['input'];

  $form['entity_carts'] = array(
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );
  // array_filter() returns only elements with actual values.
  $entity_cart_counter = 0;
  foreach (array_filter($edit['entity_carts']) as $ecid => $value) {
    $entity_cart = entity_cart_load($ecid);
    if (is_object($entity_cart) && is_numeric($entity_cart->ecid)) {
      $subject = db_query('SELECT name FROM {entity_cart} WHERE ecid = :ecid', array(':ecid' => $ecid))->fetchField();
      $form['entity_carts'][$ecid] = array(
        '#type' => 'hidden',
        '#value' => $ecid,
        '#prefix' => '<li>',
        '#suffix' => 'Order ID: ' . $ecid . ' - Buyer name: ' . check_plain($subject) . '</li>'
      );
      $entity_cart_counter++;
    }
  }
  $form['action'] = array('#type' => 'hidden', '#value' => 'delete');

  if (!$entity_cart_counter) {
    drupal_set_message(t('There do not appear to be any orders to delete, or your selected orders was deleted by another administrator.'));
    drupal_goto('admin/content/entity_cart');
  }
  else {
    return confirm_form(
      $form,
      t('Are you sure you want to delete these orders with all the details?'),
      'admin/content/entity_cart',
      t('This action cannot be undone.'),
      t('Delete orders'),
      t('Cancel')
    );
  }
}

/**
 * Process entity_cart_multiple_delete_confirm form submissions.
 */
function entity_cart_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $entities = entity_cart_load_multiple(array_keys($form_state['values']['entity_carts']));

    // Reset counter for debug information.
    $_SESSION['http_request_count'] = 0;
    batch_set(entity_cart_delete_multiple($entities));

    cache_clear_all();
    $count = count($form_state['values']['entity_carts']);
    watchdog('content', 'Deleted @count orders.', array('@count' => $count));
    drupal_set_message(format_plural($count, 'Deleted 1 order.', 'Deleted @count orders.'));
  }
  $form_state['redirect'] = 'admin/content/entity_cart';
}

/**
 * Use batch process to delete multiple entities.
 */
function entity_cart_delete_multiple($entities) {
  $operations = array();
  // Set up an operations array with 1000 elements, each doing function
  // batch_example_op_1.
  // Each operation in the operations array means at least one new HTTP request,
  // running Drupal from scratch to accomplish the operation. If the operation
  // returns with $context['finished'] != TRUE, then it will be called again.
  // In this example, $context['finished'] is always TRUE.
  foreach ($entities as $entity) {
    // Each operation is an array consisting of
    // - The function to call.
    // - An array of arguments to that function.
    $operations[] = array(
      'entity_cart_delete', array($entity, TRUE),
    );
  }
  $batch = array(
    'operations' => $operations,
  );
  return $batch;
}

/**
 * @} End of "defgroup entity_cart".
 */
